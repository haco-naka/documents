# 未来会議進捗報告
## 2017/7/6

---

# 前回
* ランチマッチングアプリを作ります！
* スケジュール作ったり管理ボード作ったり、  
  メンバー同士のイメージのすり合わせ

---

# 6/29 MTG
* 濱さんはS-CAT障害対応のため欠席  
  ![投げっぱなし](https://bytebucket.org/haco-naka/documents/raw/513687b7ae20f8b0f95da694aa1986dafe24965e/20170629.png)  
  潔い投げっぱなし

+++

* 機能一覧を洗い出す  
  ![画面構成](https://bytebucket.org/haco-naka/documents/raw/bd28980373ae2d36a05d88d21bdb2b300346010e/20170630.jpg)

+++

* GSの連携はしない
* 通知：チャットワーク
* お店データ：Yelp
* ログイン：Googleアカウント
* ログ

+++

* 共通点マッチング
  * リリース前までに下記のような項目を  
  マーケみんなに回答してもらう
    * 出身地
    * 年齢
    * たけのこきのこ
    * インドアアウトドア
    * 好きなもの（チェックボックス）
  * プロフィール画面はまだ作らない

+++

* 何で作るかも決めちゃう
  * heroku
  * html
  * PHP
    * フレームワーク Laravel + Parse
  * DB
    * back4app
  * gitLab

---

# 宿題
## 2017/7/13 まで

+++

* 環境づくり
  * スンチャン
    * MBaaSサーバ・DBを設定・設計
    * ルーティング一覧を作成
    * Controller作成
    * Viewレイアウト仕組み・ベーステンプレート定義・継承・拡張を作成

+++

* 派手なフロントのテンプレートづくり
  * なつきさん
    * 「Perfumeのグローバルサイトみたいなかんじでいいすか」「採用」  
    http://www.perfume-global.com/
    * 0%
    * 来週のMTGまでにはなんとか形にしてくれるらしい

---

# 次回MTG 7/13
* 宿題確認
* 開発入れるかな〜どうかな〜
* なつきさんにノートPCあればみんなで集まって開発できるんだけどな〜
